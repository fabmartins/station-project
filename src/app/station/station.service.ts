import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';

import { Observable } from 'rxjs/internal/Observable';

import { environment } from '../../environments/environment';


@Injectable()
export class StationService {

  constructor(private httpClient: HttpClient) { }

  public getStations(band: string): Observable<any> {
    return this.httpClient.get<any>(`${environment.apiUrl}/stations`, { params: { band: band } });
  }
}
