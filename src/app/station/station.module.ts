import { NgModule } from '@angular/core';

import { StationRoutingModule } from './station-routing.module';
import { SharedModule } from '../shared/shared.module';

import { ListStationComponent } from './list-station/list-station.component';

import { StationService } from './station.service';

@NgModule({
  imports: [
    SharedModule,
    StationRoutingModule
  ],
  declarations: [ListStationComponent],
  providers: [StationService]
})
export class StationModule { }
