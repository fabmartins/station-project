import { Component, OnInit } from '@angular/core';
import { StationService } from '../station.service';

@Component({
  selector: 'app-list-station',
  templateUrl: './list-station.component.html',
  styleUrls: ['./list-station.component.scss']
})
export class ListStationComponent implements OnInit {
  public dataSource: any;
  public displayedColumns: string[];

  constructor(private stationService: StationService) {
    this.displayedColumns = ['name', 'user', 'effectiveDate', 'endDate'];
  }

  public ngOnInit(): void {
    this.stationService.getStations('FM').subscribe((dataSource: any) => {
      this.dataSource = dataSource;
    });
  }
}
